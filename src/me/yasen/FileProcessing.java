package me.yasen;

import java.io.*;

/**
 * Created by ygeorgiev on 1/11/17.
 */
public class FileProcessing {
//    public void write(String fileName, User object) throws IOException {
//        FileOutputStream out = new FileOutputStream(fileName);
//        out.write( (
//                object.getUsername() + "$" + object.getPassword() + "$" +
//        ).getBytes());
//        out.close();
//    }
//
//    public User read(String fileName) throws Exception {
//        FileReader in = new FileReader(fileName);
//        BufferedReader reader = new BufferedReader(in);
//        String line = null;
//
//        while ((line = reader.readLine()) != null) {
//            String[] lineParts = line.split("\\$");
//            User user = new User(lineParts[0], lineParts[1], Integer.parseInt(lineParts[2]));
//
//            return user;
//        }
//
//        throw new Exception();
//    }

    public void write(String fileName, User object) throws IOException {
        FileOutputStream out = new FileOutputStream(fileName);

        ObjectOutputStream objOut = new ObjectOutputStream(out);
        objOut.writeObject(object);
        objOut.close();

        out.close();
    }

    public User read(String fileName) throws Exception {
        FileInputStream in = new FileInputStream(fileName);

        ObjectInputStream objOut = new ObjectInputStream(in);
        User object = (User) objOut.readObject();
        objOut.close();

        in.close();

        return object;
    }


}
